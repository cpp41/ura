#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <SunSet.h>
using namespace std;
#define ONE_HOUR	(60 * 60)
#define LATITUDE	46.0569465  // add your coordinates
#define LONGITUDE	14.50575149 // add your coordinates
#define PI 3.14159265

// path to diary
QString todaysChors = "/.cache/dnevnik";
// path to weather line
QString weather = "/.cache/vreme";
// path to font icons
QString ikons_for_watch = "/.cache/ikone_za_uro/";

int radij = 185; // kot kaze samo nek placeholder, v pixlih
int center_x = 225; // placeholder
int center_y = 225; // placeholder
int hour_number_font_size = 10; // okoli tega se bom kar vrtel
double plac;
QPoint p1, p2;
double sunrise, sunset, sunriseUTC, sunsetUTC;
int moonphase, x, y;
int resajz();
QString ikona = "";

QPen cyanpen(Qt::cyan);
QPen greenpen(Qt::green);
QPen blackpen(Qt::black);
QPen redpen(Qt::red);
QPen whitepen(Qt::white);
QPen yellowpen(Qt::yellow);
QPen orangepen;
QPen darkgreenpen;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // for transparent background
    setStyleSheet("background:transparent;");
    //setStyleSheet("background-color: rgba(0,0,0,200);");
    setAttribute(Qt::WA_TranslucentBackground);
    setWindowFlags(Qt::FramelessWindowHint);

    // for timer
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(MyTimer()));
    timer->start(2000);
    setMouseTracking(true);


}

void MainWindow::MyTimer()
{

    this->repaint();
}

int resajz()
{
    // for width and height
    QDesktopWidget dw;
    x=dw.width();
    y=dw.height();
    if (y <= x) {
        // presumably border of 1 pixel width, times two
        plac=y-2;
    } else {
        plac=x-2;
    }
    return plac;
}

double hoursToDegreese(int ura_v_minutah) {
    double stopinjice;
    stopinjice=((ura_v_minutah*360)/1440);

    if (stopinjice >= 360) {stopinjice = stopinjice - 360;}
    if (stopinjice >= 0 && stopinjice < 270) {stopinjice = 270 - stopinjice;}
    if (stopinjice >= 270 && stopinjice < 360) {stopinjice = 360 - (stopinjice - 270);}
    return stopinjice;
}

void getPointP(double cas_v_minutah, double radijus, int enaalidva)
{
    cas_v_minutah = cas_v_minutah / 4;
    cas_v_minutah = cas_v_minutah + 180;
    cas_v_minutah = cas_v_minutah * PI / 180;
    if (enaalidva == 1) {
        p1.setX(center_x + sin ( PI - cas_v_minutah) * radijus);
        p1.setY(center_y + cos ( PI - cas_v_minutah) * radijus);
    }
    else {
        p2.setX(center_x + sin ( PI - cas_v_minutah) * radijus);
        p2.setY(center_y + cos ( PI - cas_v_minutah) * radijus);
    }
}

void MainWindow::paintEvent(QPaintEvent *) {



    QPainter painter(this);

    plac = resajz();
    this->resize(plac,plac); // on phone/tablet, catch landscape from portraite switch, what changes?

    double puoPlaca= plac / 2;
    double radij_double = 0.90 * puoPlaca;
    double left_for_drawing = puoPlaca - radij_double;
    radij = (int)radij_double; // pixlov
    center_x = puoPlaca;
    center_y = puoPlaca;
    double hourNumberFontSizeDouble = (0.05 * puoPlaca)/2;
    hour_number_font_size = (int)hourNumberFontSizeDouble + 1;

    // for sunset and sunrise
    SunSet sun;

    auto rightnow = std::time(nullptr);
    struct tm *tad = std::localtime(&rightnow);

    sun.setPosition(LATITUDE, LONGITUDE, tad->tm_gmtoff / ONE_HOUR);
    sun.setCurrentDate(tad->tm_year + 1900, tad->tm_mon + 1, tad->tm_mday);
    sunrise = sun.calcSunrise();
    sunset = sun.calcSunset();
    sunriseUTC = sun.calcSunriseUTC();
    sunsetUTC = sun.calcSunsetUTC();
    moonphase = sun.moonPhase(static_cast<int>(rightnow));

    // css
    cyanpen.setWidth(3);
    greenpen.setWidth(1);
    blackpen.setWidth(2);
    redpen.setWidth(3);
    whitepen.setWidth(2);
    yellowpen.setWidth(1);
    orangepen.setColor("orange");
    orangepen.setWidth(1);
    darkgreenpen.setColor("darkgreen");
    darkgreenpen.setWidth(2);
    // css
    QColor backgroundColor = "black";
    backgroundColor.setAlpha(140);

    // circle
    int xi = (int)puoPlaca-radij;
    int yi = plac-2*xi;
    painter.setBrush(backgroundColor);
    painter.drawEllipse(QRect(xi,xi,yi,yi));

    int ura_vzhoda=(sunrise / 60);
    int ura_zahoda=(sunset / 60);
    int minuta_vzhoda=(int)sunrise%60;
    int minuta_zahoda=(int)sunset%60;

    int ura_vzhoda_v_minutah = (ura_vzhoda * 60) + minuta_vzhoda;
    int ura_zahoda_v_minutah = (ura_zahoda * 60) + minuta_zahoda;

    QTime ura = QTime::currentTime();
    int ura_zdaj = ura.hour();
    int minuta_zdaj = ura.minute();
    int cas_zdaj_v_minutah = ((ura_zdaj * 60) + minuta_zdaj);
//  sunrise
    getPointP(ura_vzhoda_v_minutah, radij* 9/10, 1);     // 1st point
    getPointP(ura_vzhoda_v_minutah, radij+left_for_drawing, 2);     // 2nd point
    painter.setPen(darkgreenpen); // colour, thicknes, css...
    painter.drawLine(p1,p2);  // draw line
    painter.setPen(greenpen); // colour, thicknes, css...
    painter.drawLine(p1,p2);  // draw line
//  sunset
    getPointP(ura_zahoda_v_minutah, radij* 9/10, 1);
    getPointP(ura_zahoda_v_minutah, radij+left_for_drawing, 2);
    painter.setPen(darkgreenpen);
    painter.drawLine(p1,p2);
    painter.setPen(greenpen);
    painter.drawLine(p1,p2);
//  time now
    getPointP(cas_zdaj_v_minutah, radij* 6/8, 1);
    getPointP(cas_zdaj_v_minutah, radij+left_for_drawing, 2);
    painter.setPen(redpen);
    painter.drawLine(p1,p2);
    painter.setPen(yellowpen);
    painter.drawLine(p1,p2);

    double us_pou = hour_number_font_size / 2;
    double us_tretina = hour_number_font_size / 3;
    double us_cetrt = hour_number_font_size / 4;
    double us_sest = hour_number_font_size / 6;

    // hour numbers and lines
    for( int u = 0; u < 24;  u++ ) {
        int u0 = u*60;
        int u10 = u0+10;
        int u20 = u0+20;
        int u30 = u0+30;
        int u40 = u0+40;
        int u50 = u0+50;
        QString xcv = QString::number(u);
        getPointP(u0, radij+hour_number_font_size + us_pou, 2);

        switch (u) {
            case 0:
                p2.setX(p2.x() - us_pou + us_sest);
                p2.setY(p2.y() + hour_number_font_size + us_cetrt);
                break;

            case 1:
                p2.setX(p2.x() - us_pou - us_cetrt);
                p2.setY(p2.y() + hour_number_font_size + us_cetrt);
                break;

            case 2:
                p2.setX(p2.x() - us_pou - us_tretina);
                p2.setY(p2.y() + hour_number_font_size + us_tretina);
                break;

            case 3:
                p2.setX(p2.x() - hour_number_font_size);
                p2.setY(p2.y() + hour_number_font_size);
                break;

            case 4:
                p2.setX(p2.x() - hour_number_font_size - us_cetrt);
                p2.setY(p2.y() + hour_number_font_size);
                break;

            case 5:
                p2.setX(p2.x() - hour_number_font_size - us_tretina);
                p2.setY(p2.y() + hour_number_font_size);
                break;

            case 6:
                p2.setX(p2.x() - hour_number_font_size);
                p2.setY(p2.y() + us_pou);
                break;

            case 7:
                p2.setX(p2.x() - hour_number_font_size - us_tretina);
                p2.setY(p2.y());
                break;

            case 8:
                p2.setX(p2.x() - hour_number_font_size - us_cetrt);
                p2.setY(p2.y());
                break;

            case 9:
                p2.setX(p2.x() - hour_number_font_size);
                p2.setY(p2.y());
                break;

            case 10:
                p2.setX(p2.x() - hour_number_font_size);
                p2.setY(p2.y() - us_tretina);
                break;

            case 11:
                p2.setX(p2.x() - us_pou - us_cetrt);
                p2.setY(p2.y() - us_cetrt);
                break;

            case 12:
                p2.setX(p2.x() - us_pou - us_tretina);
                p2.setY(p2.y() - us_cetrt);
                break;

            case 13:
                p2.setX(p2.x() - us_cetrt);
                p2.setY(p2.y() - us_cetrt);
                break;

            case 14:
                p2.setX(p2.x());
                p2.setY(p2.y() - us_tretina);
                break;

            case 15:
                p2.setX(p2.x() + us_cetrt);
                p2.setY(p2.y());
                break;

            case 16:
                p2.setX(p2.x() + us_cetrt);
                p2.setY(p2.y());
                break;

            case 17:
                p2.setX(p2.x() + us_tretina);
                p2.setY(p2.y());
                break;

            case 18:
                p2.setX(p2.x() + us_cetrt);
                p2.setY(p2.y() + us_pou);
                break;

            case 19:
                p2.setX(p2.x() + us_tretina);
                p2.setY(p2.y() + hour_number_font_size);
                break;

            case 20:
                p2.setX(p2.x() + us_cetrt);
                p2.setY(p2.y() + hour_number_font_size);
                break;

            case 21:
                p2.setX(p2.x() + us_cetrt);
                p2.setY(p2.y() + hour_number_font_size);
                break;

            case 22:
                p2.setX(p2.x());
                p2.setY(p2.y() + hour_number_font_size + us_tretina);
                break;

            case 23:
                p2.setX(p2.x() - us_cetrt);
                p2.setY(p2.y() + hour_number_font_size + us_cetrt);
                break;

            default:
                p2.setX(p2.x()+14);
        }

        painter.setPen(blackpen);
        ///////////////    set font
        painter.setFont({"Helvetica", hour_number_font_size+1});
        painter.drawText(p2,xcv);

        painter.setPen(orangepen);
        ///////////////    set font
        painter.setFont({"Helvetica", hour_number_font_size});
        painter.drawText(p2,xcv);

        getPointP(u0, radij+1, 1);
        getPointP(u0, radij+15, 2);
        blackpen.setWidth(3);
        painter.setPen(blackpen);
        painter.drawLine(p1,p2);
        painter.setPen(orangepen);
        painter.drawLine(p1,p2);
        getPointP(u10, radij+1, 1);
        getPointP(u10, radij+5, 2);
        blackpen.setWidth(3);
        painter.setPen(blackpen);
        painter.drawLine(p1,p2);
        painter.setPen(orangepen);
        painter.drawLine(p1,p2);
        getPointP(u20, radij+1, 1);
        getPointP(u20, radij+5, 2);
        blackpen.setWidth(3);
        painter.setPen(blackpen);
        painter.drawLine(p1,p2);
        painter.setPen(orangepen);
        painter.drawLine(p1,p2);
        getPointP(u30, radij+1, 1);
        getPointP(u30, radij+10, 2);
        blackpen.setWidth(3);
        painter.setPen(blackpen);
        painter.drawLine(p1,p2);
        painter.setPen(orangepen);
        painter.drawLine(p1,p2);
        getPointP(u40, radij+1, 1);
        getPointP(u40, radij+5, 2);
        blackpen.setWidth(3);
        painter.setPen(blackpen);
        painter.drawLine(p1,p2);
        painter.setPen(orangepen);
        painter.drawLine(p1,p2);
        getPointP(u50, radij+1, 1);
        getPointP(u50, radij+5, 2);
        blackpen.setWidth(3);
        painter.setPen(blackpen);
        painter.drawLine(p1,p2);
        painter.setPen(orangepen);
        painter.drawLine(p1,p2);
    }

    QString homePath = QDir::homePath();

    QFile dnevnik(homePath + todaysChors);
    if(dnevnik.open(QIODevice::ReadOnly)){
        QTextStream in(&dnevnik);
        getPointP((9.75)*60, radij, 1);

        painter.setPen(greenpen);
        ///////////////    set font
        painter.setFont({"Mono", (int)(hour_number_font_size/1.5)});

        int tr = 0;
        while(!in.atEnd()) {
            QString line = in.readLine();
            painter.drawText(p1.x()+20,p1.y()+60+tr,line);
            tr = tr + 16;
        }

        dnevnik.close();
    }


    QFile vreme(homePath + weather);
    if(vreme.open(QIODevice::ReadOnly)) {
        QTextStream inor(&vreme);
        getPointP((10.35)*60, radij, 1);

        painter.setPen(greenpen);
        ///////////////    some icon fonts
        painter.setFont({"Meslo", (int)(hour_number_font_size*2)});

        while(!inor.atEnd()) {
            QString line = inor.readLine();
            painter.drawText(p1.x(),p1.y()+60,line);
        }

        vreme.close();
    }






    ////////////////     ICONS
    painter.setPen(whitepen);
    ///////////////    some icon fonts
    painter.setFont({"Meslo", (int)(hour_number_font_size)});

    QFile ikona_0000(homePath + ikons_for_watch + "ikona_0000");
    if(ikona_0000.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0000);
        getPointP((0)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0000.close();
    }

    QFile ikona_0010(homePath + ikons_for_watch + "ikona_0010");
    if(ikona_0010.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0010);
        getPointP((0.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0010.close();
    }

    QFile ikona_0020(homePath + ikons_for_watch + "ikona_0020");
    if(ikona_0020.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0020);
        getPointP((0.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0020.close();
    }


    QFile ikona_0030(homePath + ikons_for_watch + "ikona_0030");
    if(ikona_0030.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0030);
        getPointP((0.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0030.close();
    }

    QFile ikona_0040(homePath + ikons_for_watch + "ikona_0040");
    if(ikona_0040.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0040);
        getPointP((0.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0040.close();
    }


    QFile ikona_0050(homePath + ikons_for_watch + "ikona_0050");
    if(ikona_0050.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0050);
        getPointP((0.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0050.close();
    }


    QFile ikona_0100(homePath + ikons_for_watch + "ikona_0100");
    if(ikona_0100.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0100);
        getPointP((1)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0100.close();
    }




    QFile ikona_0110(homePath + ikons_for_watch + "ikona_0110");
    if(ikona_0110.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0110);
        getPointP((1.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0110.close();
    }

    QFile ikona_0120(homePath + ikons_for_watch + "ikona_0120");
    if(ikona_0120.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0120);
        getPointP((1.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0120.close();
    }


    QFile ikona_0130(homePath + ikons_for_watch + "ikona_0130");
    if(ikona_0130.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0130);
        getPointP((1.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0130.close();
    }

    QFile ikona_0140(homePath + ikons_for_watch + "ikona_0140");
    if(ikona_0140.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0140);
        getPointP((1.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0140.close();
    }


    QFile ikona_0150(homePath + ikons_for_watch + "ikona_0150");
    if(ikona_0150.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0150);
        getPointP((1.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0150.close();
    }




////////////////////


    QFile ikona_0200(homePath + ikons_for_watch + "ikona_0200");
    if(ikona_0200.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0200);
        getPointP((2)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.2),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0200.close();
    }


    QFile ikona_0210(homePath + ikons_for_watch + "ikona_0210");
    if(ikona_0210.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0210);
        getPointP((2.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.2),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0210.close();
    }

    QFile ikona_0220(homePath + ikons_for_watch + "ikona_0220");
    if(ikona_0220.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0220);
        getPointP((2.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.2),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0220.close();
    }


    QFile ikona_0230(homePath + ikons_for_watch + "ikona_0230");
    if(ikona_0230.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0230);
        getPointP((2.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.35),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0230.close();
    }


    QFile ikona_0240(homePath + ikons_for_watch + "ikona_0240");
    if(ikona_0240.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0240);
        getPointP((2.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0240.close();
    }



    QFile ikona_0250(homePath + ikons_for_watch + "ikona_0250");
    if(ikona_0250.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0250);
        getPointP((2.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0250.close();
    }





///////
    QFile ikona_0300(homePath + ikons_for_watch + "ikona_0300");
    if(ikona_0300.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0300);
        getPointP((3)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0300.close();
    }

    QFile ikona_0310(homePath + ikons_for_watch + "ikona_0310");
    if(ikona_0310.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0310);
        getPointP((3.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0310.close();
    }

    QFile ikona_0320(homePath + ikons_for_watch + "ikona_0320");
    if(ikona_0320.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0320);
        getPointP((3.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.6),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_0320.close();
    }

    QFile ikona_0330(homePath + ikons_for_watch + "ikona_0330");
    if(ikona_0330.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0330);
        getPointP((3.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()-(hour_number_font_size*0.3),lineiko);
        ikona_0330.close();
    }

    QFile ikona_0340(homePath + ikons_for_watch + "ikona_0340");
    if(ikona_0340.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0340);
        getPointP((3.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y(),lineiko);
        ikona_0340.close();
    }
    QFile ikona_0350(homePath + ikons_for_watch + "ikona_0350");
    if(ikona_0350.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0350);
        getPointP((3.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y(),lineiko);
        ikona_0350.close();
    }



    ///////
    QFile ikona_0400(homePath + ikons_for_watch + "ikona_0400");
    if(ikona_0400.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0400);
        getPointP((4)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y(),lineiko);
        ikona_0400.close();
    }

    QFile ikona_0410(homePath + ikons_for_watch + "ikona_0410");
    if(ikona_0410.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0410);
        getPointP((4.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y(),lineiko);
        ikona_0410.close();
    }

    QFile ikona_0420(homePath + ikons_for_watch + "ikona_0420");
    if(ikona_0420.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0420);
        getPointP((4.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y(),lineiko);
        ikona_0420.close();
    }


    QFile ikona_0430(homePath + ikons_for_watch + "ikona_0430");
    if(ikona_0430.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0430);
        getPointP((4.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.86),p1.y()+(hour_number_font_size*0.166),lineiko);
        ikona_0430.close();
    }



    QFile ikona_0440(homePath + ikons_for_watch + "ikona_0440");
    if(ikona_0440.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0440);
        getPointP((4.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.33),lineiko);
        ikona_0440.close();
    }

    QFile ikona_0450(homePath + ikons_for_watch + "ikona_0450");
    if(ikona_0450.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0450);
        getPointP((4.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.33),lineiko);
        ikona_0450.close();
    }






/////


    QFile ikona_0500(homePath + ikons_for_watch + "ikona_0500");
    if(ikona_0500.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0500);
        getPointP((5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.33),lineiko);
        ikona_0500.close();
    }



    QFile ikona_0510(homePath + ikons_for_watch + "ikona_0510");
    if(ikona_0510.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0510);
        getPointP((5.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.33),lineiko);
        ikona_0510.close();
    }



    QFile ikona_0520(homePath + ikons_for_watch + "ikona_0520");
    if(ikona_0520.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0520);
        getPointP((5.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.33),lineiko);
        ikona_0520.close();
    }



    QFile ikona_0530(homePath + ikons_for_watch + "ikona_0530");
    if(ikona_0530.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0530);
        getPointP((5.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.415),lineiko);
        ikona_0530.close();
    }



    QFile ikona_0540(homePath + ikons_for_watch + "ikona_0540");
    if(ikona_0540.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0540);
        getPointP((5.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_0540.close();
    }



    QFile ikona_0550(homePath + ikons_for_watch + "ikona_0550");
    if(ikona_0550.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0550);
        getPointP((5.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_0550.close();
    }




/////
    QFile ikona_0600(homePath + ikons_for_watch + "ikona_0600");
    if(ikona_0600.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0600);
        getPointP((6)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_0600.close();
    }

    QFile ikona_0610(homePath + ikons_for_watch + "ikona_0610");
    if(ikona_0610.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0610);
        getPointP((6.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_0610.close();
    }

    QFile ikona_0620(homePath + ikons_for_watch + "ikona_0620");
    if(ikona_0620.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0620);
        getPointP((6.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+hour_number_font_size,p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_0620.close();
    }



    QFile ikona_0630(homePath + ikons_for_watch + "ikona_0630");
    if(ikona_0630.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0630);
        getPointP((6.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.7),p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_0630.close();
    }

    QFile ikona_0640(homePath + ikons_for_watch + "ikona_0640");
    if(ikona_0640.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0640);
        getPointP((6.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_0640.close();
    }

    QFile ikona_0650(homePath + ikons_for_watch + "ikona_0650");
    if(ikona_0650.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0650);
        getPointP((6.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_0650.close();
    }




/////
    QFile ikona_0700(homePath + ikons_for_watch + "ikona_0700");
    if(ikona_0700.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0700);
        getPointP((7)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_0700.close();
    }


    QFile ikona_0710(homePath + ikons_for_watch + "ikona_0710");
    if(ikona_0710.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0710);
        getPointP((7.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_0710.close();
    }

    QFile ikona_0720(homePath + ikons_for_watch + "ikona_0720");
    if(ikona_0720.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0720);
        getPointP((7.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_0720.close();
    }


    QFile ikona_0730(homePath + ikons_for_watch + "ikona_0730");
    if(ikona_0730.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0730);
        getPointP((7.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.87),p1.y()+(hour_number_font_size*0.833),lineiko);
        ikona_0730.close();
    }


    QFile ikona_0740(homePath + ikons_for_watch + "ikona_0740");
    if(ikona_0740.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0740);
        getPointP((7.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size),p1.y()+(hour_number_font_size),lineiko);
        ikona_0740.close();
    }

    QFile ikona_0750(homePath + ikons_for_watch + "ikona_0750");
    if(ikona_0750.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0750);
        getPointP((7.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size),p1.y()+(hour_number_font_size),lineiko);
        ikona_0750.close();
    }

/////
    QFile ikona_0800(homePath + ikons_for_watch + "ikona_0800");
    if(ikona_0800.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0800);
        getPointP((8)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size),p1.y()+(hour_number_font_size),lineiko);
        ikona_0800.close();
    }


    QFile ikona_0810(homePath + ikons_for_watch + "ikona_0810");
    if(ikona_0810.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0810);
        getPointP((8.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size),p1.y()+(hour_number_font_size),lineiko);
        ikona_0810.close();
    }

    QFile ikona_0820(homePath + ikons_for_watch + "ikona_0820");
    if(ikona_0820.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0820);
        getPointP((8.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size),p1.y()+(hour_number_font_size),lineiko);
        ikona_0820.close();
    }


    QFile ikona_0830(homePath + ikons_for_watch + "ikona_0830");
    if(ikona_0830.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0830);
        getPointP((8.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.1),lineiko);
        ikona_0830.close();
    }


    QFile ikona_0840(homePath + ikons_for_watch + "ikona_0840");
    if(ikona_0840.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0840);
        getPointP((8.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_0840.close();
    }


    QFile ikona_0850(homePath + ikons_for_watch + "ikona_0850");
    if(ikona_0850.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0850);
        getPointP((8.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_0850.close();
    }



//////////
    QFile ikona_0900(homePath + ikons_for_watch + "ikona_0900");
    if(ikona_0900.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0900);
        getPointP((9)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_0900.close();
    }

    QFile ikona_0910(homePath + ikons_for_watch + "ikona_0910");
    if(ikona_0910.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0910);
        getPointP((9.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_0910.close();
    }

    QFile ikona_0920(homePath + ikons_for_watch + "ikona_0920");
    if(ikona_0920.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0920);
        getPointP((9.33)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_0920.close();
    }


    QFile ikona_0930(homePath + ikons_for_watch + "ikona_0930");
    if(ikona_0930.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0930);
        getPointP((9.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.45),lineiko);
        ikona_0930.close();
    }


    QFile ikona_0940(homePath + ikons_for_watch + "ikona_0940");
    if(ikona_0940.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0940);
        getPointP((9.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_0940.close();
    }

    QFile ikona_0950(homePath + ikons_for_watch + "ikona_0950");
    if(ikona_0950.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_0950);
        getPointP((9.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_0950.close();
    }


//////////
    QFile ikona_1000(homePath + ikons_for_watch + "ikona_1000");
    if(ikona_1000.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1000);
        getPointP((10)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_1000.close();
    }

    QFile ikona_1010(homePath + ikons_for_watch + "ikona_1010");
    if(ikona_1010.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1010);
        getPointP((10.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_1010.close();
    }

    QFile ikona_1020(homePath + ikons_for_watch + "ikona_1020");
    if(ikona_1020.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1020);
        getPointP((10.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_1020.close();
    }


    QFile ikona_1030(homePath + ikons_for_watch + "ikona_1030");
    if(ikona_1030.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1030);
        getPointP((10.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.25),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_1030.close();
    }

    QFile ikona_1040(homePath + ikons_for_watch + "ikona_1040");
    if(ikona_1040.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1040);
        getPointP((10.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()+(hour_number_font_size*1.6),lineiko);
        ikona_1040.close();
    }

    QFile ikona_1050(homePath + ikons_for_watch + "ikona_1050");
    if(ikona_1050.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1050);
        getPointP((10.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1050.close();
    }







//////////

    QFile ikona_1100(homePath + ikons_for_watch + "ikona_1100");
    if(ikona_1100.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1100);
        getPointP((11)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1100.close();
    }

    QFile ikona_1110(homePath + ikons_for_watch + "ikona_1110");
    if(ikona_1110.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1110);
        getPointP((11.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()+(hour_number_font_size*1.8),lineiko);
        ikona_1110.close();
    }

    QFile ikona_1120(homePath + ikons_for_watch + "ikona_1120");
    if(ikona_1120.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1120);
        getPointP((11.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()+(hour_number_font_size*1.8),lineiko);
        ikona_1120.close();
    }


    QFile ikona_1130(homePath + ikons_for_watch + "ikona_1130");
    if(ikona_1130.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1130);
        getPointP((11.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.1),p1.y()+(hour_number_font_size*1.8),lineiko);
        ikona_1130.close();
    }

    QFile ikona_1140(homePath + ikons_for_watch + "ikona_1140");
    if(ikona_1140.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1140);
        getPointP((11.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.1)-(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1140.close();
    }

    QFile ikona_1150(homePath + ikons_for_watch + "ikona_1150");
    if(ikona_1150.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1150);
        getPointP((11.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.1)-(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1150.close();
    }



/////////////
    QFile ikona_1200(homePath + ikons_for_watch + "ikona_1200");
    if(ikona_1200.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1200);
        getPointP((12)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.1)-(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1200.close();
    }

    QFile ikona_1210(homePath + ikons_for_watch + "ikona_1210");
    if(ikona_1210.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1210);
        getPointP((12.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.1)-(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1210.close();
    }

    QFile ikona_1220(homePath + ikons_for_watch + "ikona_1220");
    if(ikona_1220.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1220);
        getPointP((12.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.1)-(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1220.close();
    }


    QFile ikona_1230(homePath + ikons_for_watch + "ikona_1230");
    if(ikona_1230.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1230);
        getPointP((12.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()+(hour_number_font_size*0.1)-(hour_number_font_size*0.5),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1230.close();
    }


    QFile ikona_1240(homePath + ikons_for_watch + "ikona_1240");
    if(ikona_1240.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1240);
        getPointP((12.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1240.close();
    }
    QFile ikona_1250(homePath + ikons_for_watch + "ikona_1250");
    if(ikona_1250.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1250);
        getPointP((12.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1250.close();
    }




/////////////
    QFile ikona_1300(homePath + ikons_for_watch + "ikona_1300");
    if(ikona_1300.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1300);
        getPointP((13)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1300.close();
    }


    QFile ikona_1310(homePath + ikons_for_watch + "ikona_1310");
    if(ikona_1310.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1310);
        getPointP((13.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1310.close();
    }

    QFile ikona_1320(homePath + ikons_for_watch + "ikona_1320");
    if(ikona_1320.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1320);
        getPointP((13.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.7),lineiko);
        ikona_1320.close();
    }

    QFile ikona_1330(homePath + ikons_for_watch + "ikona_1330");
    if(ikona_1330.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1330);
        getPointP((13.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.75),p1.y()+(hour_number_font_size*1.6),lineiko);
        ikona_1330.close();
    }


    QFile ikona_1340(homePath + ikons_for_watch + "ikona_1340");
    if(ikona_1340.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1340);
        getPointP((13.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_1340.close();
    }
    QFile ikona_1350(homePath + ikons_for_watch + "ikona_1350");
    if(ikona_1350.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1350);
        getPointP((13.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_1350.close();
    }


/////////////
    QFile ikona_1400(homePath + ikons_for_watch + "ikona_1400");
    if(ikona_1400.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1400);
        getPointP((14)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_1400.close();
    }


    QFile ikona_1410(homePath + ikons_for_watch + "ikona_1410");
    if(ikona_1410.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1410);
        getPointP((14.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_1410.close();
    }

    QFile ikona_1420(homePath + ikons_for_watch + "ikona_1420");
    if(ikona_1420.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1420);
        getPointP((14.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_1420.close();
    }

    QFile ikona_1430(homePath + ikons_for_watch + "ikona_1430");
    if(ikona_1430.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1430);
        getPointP((14.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()+(hour_number_font_size*1.5),lineiko);
        ikona_1430.close();
    }

    QFile ikona_1440(homePath + ikons_for_watch + "ikona_1440");
    if(ikona_1440.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1440);
        getPointP((14.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.25),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_1440.close();
    }

    QFile ikona_1450(homePath + ikons_for_watch + "ikona_1450");
    if(ikona_1450.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1450);
        getPointP((14.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.25),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_1450.close();
    }



/////////////
    QFile ikona_1500(homePath + ikons_for_watch + "ikona_1500");
    if(ikona_1500.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1500);
        getPointP((15)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.25),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_1500.close();
    }

    QFile ikona_1510(homePath + ikons_for_watch + "ikona_1510");
    if(ikona_1510.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1510);
        getPointP((15.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.25),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_1510.close();
    }

    QFile ikona_1520(homePath + ikons_for_watch + "ikona_1520");
    if(ikona_1520.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1520);
        getPointP((15.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.25),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_1520.close();
    }

    QFile ikona_1530(homePath + ikons_for_watch + "ikona_1530");
    if(ikona_1530.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1530);
        getPointP((15.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.25),p1.y()+(hour_number_font_size*1.25),lineiko);
        ikona_1530.close();
    }

    QFile ikona_1540(homePath + ikons_for_watch + "ikona_1540");
    if(ikona_1540.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1540);
        getPointP((15.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.5),p1.y()+(hour_number_font_size),lineiko);
        ikona_1540.close();
    }

    QFile ikona_1550(homePath + ikons_for_watch + "ikona_1550");
    if(ikona_1550.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1550);
        getPointP((15.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.5),p1.y()+(hour_number_font_size),lineiko);
        ikona_1550.close();
    }


/////////////
    QFile ikona_1600(homePath + ikons_for_watch + "ikona_1600");
    if(ikona_1600.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1600);
        getPointP((16)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.5),p1.y()+(hour_number_font_size),lineiko);
        ikona_1600.close();
    }

    QFile ikona_1610(homePath + ikons_for_watch + "ikona_1610");
    if(ikona_1610.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1610);
        getPointP((16.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.5),p1.y()+(hour_number_font_size),lineiko);
        ikona_1610.close();
    }

    QFile ikona_1620(homePath + ikons_for_watch + "ikona_1620");
    if(ikona_1620.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1620);
        getPointP((16.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.5),p1.y()+(hour_number_font_size),lineiko);
        ikona_1620.close();
    }


    QFile ikona_1630(homePath + ikons_for_watch + "ikona_1630");
    if(ikona_1630.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1630);
        getPointP((16.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.4),p1.y()+(hour_number_font_size*0.75),lineiko);
        ikona_1630.close();
    }


    QFile ikona_1640(homePath + ikons_for_watch + "ikona_1640");
    if(ikona_1640.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1640);
        getPointP((16.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_1640.close();
    }

    QFile ikona_1650(homePath + ikons_for_watch + "ikona_1650");
    if(ikona_1650.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1650);
        getPointP((16.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_1650.close();
    }






/////////////
    QFile ikona_1700(homePath + ikons_for_watch + "ikona_1700");
    if(ikona_1700.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1700);
        getPointP((17)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_1700.close();
    }


    QFile ikona_1710(homePath + ikons_for_watch + "ikona_1710");
    if(ikona_1710.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1710);
        getPointP((17.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_1710.close();
    }

    QFile ikona_1720(homePath + ikons_for_watch + "ikona_1720");
    if(ikona_1720.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1720);
        getPointP((17.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_1720.close();
    }



    QFile ikona_1730(homePath + ikons_for_watch + "ikona_1730");
    if(ikona_1730.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1730);
        getPointP((17.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.66),lineiko);
        ikona_1730.close();
    }



    QFile ikona_1740(homePath + ikons_for_watch + "ikona_1740");
    if(ikona_1740.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1740);
        getPointP((17.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*2),p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_1740.close();
    }


    QFile ikona_1750(homePath + ikons_for_watch + "ikona_1750");
    if(ikona_1750.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1750);
        getPointP((17.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*2),p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_1750.close();
    }



/////////////
    QFile ikona_1800(homePath + ikons_for_watch + "ikona_1800");
    if(ikona_1800.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1800);
        getPointP((18)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*2),p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_1800.close();
    }

    QFile ikona_1810(homePath + ikons_for_watch + "ikona_1810");
    if(ikona_1810.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1810);
        getPointP((18.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*2),p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_1810.close();
    }
    QFile ikona_1820(homePath + ikons_for_watch + "ikona_1820");
    if(ikona_1820.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1820);
        getPointP((18.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*2),p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_1820.close();
    }

    QFile ikona_1830(homePath + ikons_for_watch + "ikona_1830");
    if(ikona_1830.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1830);
        getPointP((18.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*2),p1.y()+(hour_number_font_size*0.5),lineiko);
        ikona_1830.close();
    }

    QFile ikona_1840(homePath + ikons_for_watch + "ikona_1840");
    if(ikona_1840.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1840);
        getPointP((18.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.33),lineiko);
        ikona_1840.close();
    }

    QFile ikona_1850(homePath + ikons_for_watch + "ikona_1850");
    if(ikona_1850.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1850);
        getPointP((18.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.33),lineiko);
        ikona_1850.close();
    }


/////////////
    QFile ikona_1900(homePath + ikons_for_watch + "ikona_1900");
    if(ikona_1900.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1900);
        getPointP((19)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.33),lineiko);
        ikona_1900.close();
    }

    QFile ikona_1910(homePath + ikons_for_watch + "ikona_1910");
    if(ikona_1910.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1910);
        getPointP((19.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.33),lineiko);
        ikona_1910.close();
    }

    QFile ikona_1920(homePath + ikons_for_watch + "ikona_1920");
    if(ikona_1920.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1920);
        getPointP((19.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.33),lineiko);
        ikona_1920.close();
    }

    QFile ikona_1930(homePath + ikons_for_watch + "ikona_1930");
    if(ikona_1930.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1930);
        getPointP((19.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.75),p1.y()+(hour_number_font_size*0.166),lineiko);
        ikona_1930.close();
    }



    QFile ikona_1940(homePath + ikons_for_watch + "ikona_1940");
    if(ikona_1940.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1940);
        getPointP((19.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.5),p1.y(),lineiko);
        ikona_1940.close();
    }


    QFile ikona_1950(homePath + ikons_for_watch + "ikona_1950");
    if(ikona_1950.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_1950);
        getPointP((19.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.5),p1.y(),lineiko);
        ikona_1950.close();
    }








/////////////
    QFile ikona_2000(homePath + ikons_for_watch + "ikona_2000");
    if(ikona_2000.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2000);
        getPointP((20)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.5),p1.y(),lineiko);
        ikona_2000.close();
    }



    QFile ikona_2010(homePath + ikons_for_watch + "ikona_2010");
    if(ikona_2010.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2010);
        getPointP((20.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.5),p1.y(),lineiko);
        ikona_2010.close();
    }




    QFile ikona_2020(homePath + ikons_for_watch + "ikona_2020");
    if(ikona_2020.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2020);
        getPointP((20.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.5),p1.y(),lineiko);
        ikona_2020.close();
    }



    QFile ikona_2030(homePath + ikons_for_watch + "ikona_2030");
    if(ikona_2030.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2030);
        getPointP((20.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.2),p1.y(),lineiko);
        ikona_2030.close();
    }


    QFile ikona_2040(homePath + ikons_for_watch + "ikona_2040");
    if(ikona_2040.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2040);
        getPointP((20.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*1.1),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_2040.close();
    }


    QFile ikona_2050(homePath + ikons_for_watch + "ikona_2050");
    if(ikona_2050.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2050);
        getPointP((20.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_2050.close();
    }

/////////////

    QFile ikona_2100(homePath + ikons_for_watch + "ikona_2100");
    if(ikona_2100.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2100);
        getPointP((21)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_2100.close();
    }




    QFile ikona_2110(homePath + ikons_for_watch + "ikona_2110");
    if(ikona_2110.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2110);
        getPointP((21.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_2110.close();
    }



    QFile ikona_2120(homePath + ikons_for_watch + "ikona_2120");
    if(ikona_2120.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2120);
        getPointP((21.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_2120.close();
    }

    QFile ikona_2130(homePath + ikons_for_watch + "ikona_2130");
    if(ikona_2130.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2130);
        getPointP((21.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()-(hour_number_font_size*0.5),lineiko);
        ikona_2130.close();
    }


    QFile ikona_2140(homePath + ikons_for_watch + "ikona_2140");
    if(ikona_2140.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2140);
        getPointP((21.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2140.close();
    }

    QFile ikona_2150(homePath + ikons_for_watch + "ikona_2150");
    if(ikona_2150.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2150);
        getPointP((21.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2150.close();
    }
/////////////

    QFile ikona_2200(homePath + ikons_for_watch + "ikona_2200");
    if(ikona_2200.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2200);
        getPointP((22)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2200.close();
    }

    QFile ikona_2210(homePath + ikons_for_watch + "ikona_2210");
    if(ikona_2210.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2210);
        getPointP((22.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2210.close();
    }

    QFile ikona_2220(homePath + ikons_for_watch + "ikona_2220");
    if(ikona_2220.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2220);
        getPointP((22.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2220.close();
    }

    QFile ikona_2230(homePath + ikons_for_watch + "ikona_2230");
    if(ikona_2230.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2230);
        getPointP((22.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2230.close();
    }

    QFile ikona_2240(homePath + ikons_for_watch + "ikona_2230");
    if(ikona_2230.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2230);
        getPointP((22.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2230.close();
    }

    QFile ikona_2250(homePath + ikons_for_watch + "ikona_2250");
    if(ikona_2250.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2250);
        getPointP((22.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2250.close();
    }

/////////////
    QFile ikona_2300(homePath + ikons_for_watch + "ikona_2300");
    if(ikona_2300.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2300);
        getPointP((23)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.4),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2300.close();
    }


    QFile ikona_2310(homePath + ikons_for_watch + "ikona_2310");
    if(ikona_2310.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2310);
        getPointP((23.166)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.4),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2310.close();
    }


    QFile ikona_2320(homePath + ikons_for_watch + "ikona_2320");
    if(ikona_2320.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2320);
        getPointP((23.333)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.4),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2320.close();
    }

    QFile ikona_2330(homePath + ikons_for_watch + "ikona_2330");
    if(ikona_2330.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2330);
        getPointP((23.5)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.4),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2330.close();
    }

    QFile ikona_2340(homePath + ikons_for_watch + "ikona_2340");
    if(ikona_2340.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2340);
        getPointP((23.666)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2340.close();
    }

    QFile ikona_2350(homePath + ikons_for_watch + "ikona_2350");
    if(ikona_2350.open(QIODevice::ReadOnly)){
        QTextStream iniko(&ikona_2350);
        getPointP((23.833)*60, radij, 1);
        QString lineiko = iniko.readLine();
        painter.drawText(p1.x()-(hour_number_font_size*0.5),p1.y()-(hour_number_font_size*0.75),lineiko);
        ikona_2350.close();
    }




/////////////
}


MainWindow::~MainWindow()
{
    delete ui;
}

















































